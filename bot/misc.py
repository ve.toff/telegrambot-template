from aiogram import Bot, Dispatcher
from aiogram.utils.executor import Executor
from aiogram.contrib.fsm_storage.memory import MemoryStorage

from bot.config import (TOKEN, PROXY_URL, PROXY_AUTH, SKIP_UPDATES,
                        LOGFILE_PATH)
import logging


logging.basicConfig(level=logging.INFO, filename=LOGFILE_PATH)

bot = Bot(token=TOKEN, parse_mode='HTML',
          proxy=PROXY_URL, proxy_auth=PROXY_AUTH)

storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
executor = Executor(dp, skip_updates=SKIP_UPDATES)
