from aiogram.utils.exceptions import InvalidQueryID, MessageCantBeEdited
from bot.misc import dp


import logging

log = logging.getLogger(__name__)


@dp.errors_handler(exception=InvalidQueryID)
async def InvalidQueryIDException(update, exception):
    log.error(exception)
    return True
