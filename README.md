# *TelegramBot Template*

#### Template to simplify *Telegram* bot creation.

## Installation

### Install dependencies
```
pip install -r requirements.txt
```

## Usage
### Configure bot in *config.py* and run
```
python main.py
```